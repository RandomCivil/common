package profile

import (
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"time"
)

var (
	pprofTypes = []string{"heap", "goroutine", "allocs", "threadcreate", "block", "mutex"}
)

func init() {
	runtime.SetBlockProfileRate(1)
	runtime.SetMutexProfileFraction(1)
}

func CpuPprofStat() {
	f, err := os.Create("/tmp/cpu.pprof")
	log.Println("start cpu pprof", f)
	if err != nil {
		log.Fatal("could not create profile: ", err)
	}
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Println("could not start CPU profile: ", err)
	}
	select {
	case <-time.After(60 * time.Second):
		pprof.StopCPUProfile()
		f.Close()
		return
	}
}

func PprofStat() {
	for _, t := range pprofTypes {
		f, err := os.Create("/tmp/" + t + ".pprof")
		if err != nil {
			log.Fatal("could not create profile: ", err)
		}
		defer f.Close()
		pprof.Lookup(t).WriteTo(f, 0)
	}
}
