package profile

import (
	"log"
	"os"
	"runtime/trace"
)

func DoTrace() *os.File {
	f, err := os.Create("trace.out")
	if err != nil {
		log.Fatalf("failed to create trace output file: %v", err)
	}
	if err := trace.Start(f); err != nil {
		log.Fatalf("failed to start trace: %v", err)
	}
	return f
}
