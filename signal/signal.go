package signal

import (
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/RandomCivil/common/profile"
)

var isTestEnv bool

func init() {
	if flag.Lookup("test.v") != nil {
		isTestEnv = true
	}
}

type Signal struct {
	Quit  chan bool
	SQuit chan os.Signal
	Hup   chan os.Signal
}

func NewSignal() *Signal {
	s := &Signal{
		Quit:  make(chan bool),
		SQuit: make(chan os.Signal),
		Hup:   make(chan os.Signal),
	}
	signal.Notify(s.SQuit, os.Interrupt, syscall.SIGTERM)
	signal.Notify(s.Hup, syscall.SIGHUP)
	return s
}

func (s *Signal) Handle(l io.Closer) {
	for {
		select {
		case <-s.SQuit:
			log.Println("recv signal quit")
			l.Close()
		case <-s.Quit:
			log.Println("recv quit")
			l.Close()
		case <-s.Hup:
			log.Println("recv hup")
			if !isTestEnv {
				go func() {
					profile.PprofStat()
					profile.CpuPprofStat()
				}()
			}
		}
	}
}
