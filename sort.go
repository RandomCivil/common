package common

import "golang.org/x/exp/constraints"

type GenericsSorter[T constraints.Ordered] struct {
	Slice []T
}

func (p GenericsSorter[T]) Len() int { return len(p.Slice) }
func (p GenericsSorter[T]) Less(i, j int) bool {
	return p.Slice[i] < p.Slice[j]
}
func (p GenericsSorter[T]) Swap(i, j int) { p.Slice[i], p.Slice[j] = p.Slice[j], p.Slice[i] }

func partition[T constraints.Ordered](a []T, lo, hi int) int {
	pivot := a[hi]
	pos := lo
	for i := lo; i < hi; i++ {
		// pos<=i,最终的pos位置上是pivot
		// a[i]<pivot,说明a[i]在最终的pos前面
		if a[i] < pivot {
			a[i], a[pos] = a[pos], a[i]
			pos++
		}
	}
	a[hi], a[pos] = a[pos], a[hi]
	return pos
}

func GenericsQuickSort[T constraints.Ordered](a []T, lo, hi int) {
	if lo >= hi {
		return
	}
	i := partition(a, lo, hi)
	GenericsQuickSort(a, lo, i-1)
	GenericsQuickSort(a, i+1, hi)
}
