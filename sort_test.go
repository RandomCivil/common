package common

import (
	"sort"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGenericsSort(t *testing.T) {
	tests := []struct {
		slice  []int
		expect []int
	}{
		{[]int{1}, []int{1}},
		{[]int{1, 2, 3, 4, 5}, []int{1, 2, 3, 4, 5}},
		{[]int{5, 4, 3, 2, 1}, []int{1, 2, 3, 4, 5}},
		{[]int{1, 2, 4, 9, 6, 5}, []int{1, 2, 4, 5, 6, 9}},
		{[]int{5, 2, 4, 6, 1, 3}, []int{1, 2, 3, 4, 5, 6}},
	}

	tests1 := []struct {
		slice  []float32
		expect []float32
	}{
		{[]float32{6.2, 7.91, 2.1}, []float32{2.1, 6.2, 7.91}},
		{[]float32{2.1, 6.2, 7.91}, []float32{2.1, 6.2, 7.91}},
		{[]float32{7.91, 6.2, 2.1}, []float32{2.1, 6.2, 7.91}},
	}
	Convey("TestSort", t, func() {
		Convey("int", func() {
			for _, tt := range tests {
				input := make([]int, len(tt.slice))
				copy(input, tt.slice)

				s := GenericsSorter[int]{tt.slice}
				sort.Sort(s)
				So(tt.slice, ShouldResemble, tt.expect)

				GenericsQuickSort(input, 0, len(tt.slice)-1)
				So(tt.slice, ShouldResemble, tt.expect)
			}
		})
		Convey("float32", func() {
			for _, tt := range tests1 {
				input := make([]float32, len(tt.slice))
				copy(input, tt.slice)

				s := GenericsSorter[float32]{tt.slice}
				sort.Sort(s)
				So(tt.slice, ShouldResemble, tt.expect)

				GenericsQuickSort(input, 0, len(tt.slice)-1)
				So(tt.slice, ShouldResemble, tt.expect)
			}
		})
	})
}
