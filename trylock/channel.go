package trylock

type channelTryLock struct {
	ch chan struct{}
}

func NewChannelTryLock() *channelTryLock {
	return &channelTryLock{
		ch: make(chan struct{}, 1),
	}
}

func (l *channelTryLock) TryLock() bool {
	select {
	case l.ch <- struct{}{}:
		return true
	default:
	}
	return false
}

func (l *channelTryLock) Unlock() {
	select {
	case <-l.ch:
		return
	default:
	}
	panic("sync: unlock of unlocked mutex")
}
