package trylock

import (
	"sync"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTryLock(t *testing.T) {
	Convey("TestTryLock", t, func() {
		Convey("basic use", func() {
			for _, l := range []TryLocker{NewChannelTryLock(), NewUnsafePointerTryLock()} {
				So(l.TryLock(), ShouldBeTrue)
				So(func() { l.Unlock() }, ShouldNotPanic)
			}
		})
		Convey("repeat lock", func() {
			for _, l := range []TryLocker{NewChannelTryLock(), NewUnsafePointerTryLock()} {
				So(l.TryLock(), ShouldBeTrue)
				So(l.TryLock(), ShouldBeFalse)
				So(func() { l.Unlock() }, ShouldNotPanic)
			}
		})
		Convey("unlock error", func() {
			for _, l := range []TryLocker{NewChannelTryLock()} {
				So(func() { l.Unlock() }, ShouldPanic)
			}
		})
		Convey("complex", func() {
			for _, l := range []TryLocker{NewChannelTryLock(), NewUnsafePointerTryLock()} {
				So(l.TryLock(), ShouldBeTrue)
				So(l.TryLock(), ShouldBeFalse)
				So(func() { l.Unlock() }, ShouldNotPanic)
				So(l.TryLock(), ShouldBeTrue)
				So(func() { l.Unlock() }, ShouldNotPanic)
			}
		})
		Convey("concurrent", func() {
			for _, l := range []TryLocker{NewChannelTryLock(), NewUnsafePointerTryLock()} {
				var count [2]int
				var wg sync.WaitGroup
				wg.Add(10)
				for i := 0; i < 10; i++ {
					go func() {
						if l.TryLock() {
							count[0]++
						} else {
							count[1]++
						}
						wg.Done()
					}()
				}
				wg.Wait()
				So(count[0], ShouldEqual, 1)
				So(count[1], ShouldEqual, 9)
			}
		})
	})
}

func recoverFn(f func()) (err interface{}) {
	defer func() {
		if e := recover(); e != nil {
			err = e
		}
	}()
	f()
	return
}
