package trylock

type TryLocker interface {
	TryLock() bool
	Unlock()
}
