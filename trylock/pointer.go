package trylock

import (
	"sync"
	"sync/atomic"
	"unsafe"
)

type unsafePointerTryLock struct {
	mu sync.Mutex
}

func NewUnsafePointerTryLock() *unsafePointerTryLock {
	return new(unsafePointerTryLock)
}

func (l *unsafePointerTryLock) TryLock() bool {
	return atomic.CompareAndSwapInt32((*int32)(unsafe.Pointer(&l.mu)), 0, 1)
}

func (l *unsafePointerTryLock) Unlock() {
	l.mu.Unlock()
}
