package common

import "fmt"

var (
	GitCommit string = "unknown"
	BuildTime string = "unknown"
	GoVersion string = "unknown"
)

func Verbose() {
	fmt.Printf("%s\nbuild time: %s\ncommit: %s\n", GoVersion, BuildTime, GitCommit)
}
